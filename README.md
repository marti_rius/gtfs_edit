# gtfs_edit

## Project Description
gtfs_edit is an open-source editor for GTFS format.
It relies on LibreOffice or other spreadsheet software to edit timetables,
and on geojson.io webpage or other Geographic Information Software such QGIS
to edit stops and shapes.
It allows easy creation of high quality shapes by calling Google Directions API
(a free developer key is required, see below).
It uses the database structure provided by django-multi-gtfs
(https://github.com/tulsawebdevs/django-multi-gtfs), onto which functionality
to export GTFS data to/from spreadsheet and geojson files has been added.

## Status
Alpha: Suitable for production only if you can cope with the inconveniences of a piece of code that is not mature yet.
The repo includes a fully working example that passes GTFS Validator checks.
It supports all compulsory fields (agency, stops, calendar, routes, trips, stop_times,
frequencies) and also shapes.
It does yet not support: calendar_dates, fare_rules, fare_attributes, transfers, feed_info.

## Issues
There is a limitation for which trips that stop twice in the same stop cannot be dumped from gtfz.zip
to xlsx files. However, this will be solved in short.

## Installation steps
The following steps have been tested on an Amazon Lightsail instance OpenSuse 42.2

### Install postgres and postgis
sudo zypper --non-interactive install binutils python-pip python-virtualenv pgadmin3 \
sudo zypper addrepo http://download.opensuse.org/repositories/Application:Geo:Staging/openSUSE_Leap_42.3/Application:Geo:Staging.repo \
sudo zypper refresh \
sudo zypper install postgresql93-postgis  # press option 2 

### PostGIS database setup
Start database (useful also to start database from scratch if needed):\
sudo rm -rf /usr/local/var/postgres \
sudo mkdir /usr/local/var \
sudo chmod 777 /usr/local/var \
sudo su - postgres \
pg_ctl -D /usr/local/var/postgres -l /usr/local/var/postgres/server.log init \
/usr/lib/postgresql93/bin/pg_ctl -D /usr/local/var/postgres -l logfile start \
psql \
*Psql prompt appears. Please type the following lines* \
CREATE ROLE your_name WITH CREATEDB CREATEROLE LOGIN SUPERUSER; \
CREATE EXTENSION postgis; \
CREATE EXTENSION postgis_topology; \
*Exit psql with ctrl-D* \
createdb -O your_name template_postgis -E UTF-8 \
createlang plpgsql template_postgis \
psql -d template_postgis -f /usr/share/postgresql93/contrib/postgis-2.4/postgis.sql \
psql -d template_postgis -f /usr/share/postgresql93/contrib/postgis-2.4/spatial_ref_sys.sql \
createdb -O your_name gtfsdb  -E UTF-8 \
*Type 'exit' to leave postgres user*

### Create and activate new virtual environment
> Enviroment will be named env1 (this is optional but recommended)
> Use "deactivate" to deactivate virtualenv.
virtualenv  --python=python2.7  env1
source env1/bin/activate

### Install Python packages
pip install --upgrade pip \
git clone https://github.com/tulsawebdevs/django-multi-gtfs.git \
pip install Django==1.11.5 pandas xlrd pykml openpyxl multigtfs psycopg2-binary geojson 

### Install gtfs_edit
git clone https://bitbucket.org/marti_rius/gtfs_edit.git 

###Django secret key and Google Developer API key
Create a file that contains Django and Google Developer API keys. File is
named 'secrets.py' and must be located in gtfs_edit/gtfs_edit folder.
It must have the following content:
SECRET_KEY = 'my_django_key' \
GOOGLE_API_KEY = 'my_google_key' \
A Django secret key can be generated with the following command: \
python -c 'import random; print "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)])' \
Google key is optional, and required only in command 'update_shapes'. 

### Test with:
cd gtfs_edit \
./manage.py migrate \
./manage.py importgtfs test/zip/gtfs.zip

## Example Project
The code comes with a working example in gtfs_edit/example folder. It shows how to create a
valid gtfs feed file, and serves as a template to create your own ones.

###Example 1: from files to gtfs.zip file
Files to edit are agency_info.xlsx, stops.xlsx and route_NN.xlsx. Each one contains one or more tabs with
fields to be filled in. See GTFS specification for details. \
Routes are described by route_NN.xlsx files, where NN is a string with the route name. One file is
to be specified for each route. The tab named 'info' contains general info on the route. The rest of
tabs are named after each of the 'services' of the route. Inside, they contain a column for each of
the trips. Tips for filling-in routes are: 
* stop_id is mandatory for each stop. If stop_name is specified as well, a check is done to ensure match.
* If arrival and departure times for that stop are always the same, then a single with tag 'arr/dep'
 can be specified to avoid duplicating the time field.
* Optional fields such as pickup_type can be specified in additional rows, only if needed.
Files are parsed as follows (commands are executed from the gtfs_edit folder):
./manage.py delete_feeds "*"

#### Optional, only if stops are introduced as geojson files with QGIS software or similar:
./manage.py stops_geojson_to_xlsx \
./manage.py parse_agency_info \
./manage.py parse_stops \
./manage.py parse_route L3   -- or, alternatively --  ./manage.py parse_route "*"  

#### Optional, only if shapes are desired:
./manage.py update_shapes L3 \
./manage.py exportgtfs 1 --name gtfs.zip 

### Example 2: from gtfs.zip to xlsx files:
./manage.py delete_feeds "*" \
./manage.py importgtfs test/zip/gtfs.zip \
./manage.py dump_agency_info \
./manage.py dump_stops \
./manage.py dump_route "*" 

##Data visualization and editing with multi-django-gtfs admin system
Data in database can also be viewed and edited with: \
gtfs_edit> python ./manage.py runserver \
and opening http://127.0.0.1:8000/admin in your browser. Note that any changes will not
be synchronized with .xlsx files until you run dump_agency_info, dump_stops or dump_route.

##Troubleshooting
-- Error message: django.db.utils.OperationalError: could not connect to server: No such file or directory
        Is the server running locally and accepting
        connections on Unix domain socket "/var/run/postgresql/.s.PGSQL.5432"?
Solution is to restart postgresql server:
sudo su - postgres
/usr/lib/postgresql94/bin/pg_ctl -D /usr/local/var/postgres -l logfile start
