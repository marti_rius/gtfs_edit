from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Agency, Service, Route,
                              Stop, StopTime, Trip)
import itertools
from difflib import ndiff
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math

class Command(BaseCommand):

    help = 'Create shapes for given route(s). Uses Google Directions API to suggest a shape from the stops'

    def add_arguments(self, parser):
        parser.add_argument('route_id', nargs='+', type=str)
        #parser.add_argument('no_overwrite', nargs='+', type=str) # Do not overwrite original route.xlsx file

    def handle(self, *args, **options):
        ''' First: find unique trip-direction sets within the route.
        For each trip-direction set, pass stops to Google Directions API to get the shape.
        Then show a preview for the shape, and save as KML.
        It can later be edited with QGIS.'''
        outdir = os.path.join(settings.GTFS_FOLDER)
        route_id = options['route_id']
        xlsx_file = os.path.join(outdir,'route_' + options['route_id'][0] + '.xlsx')
        from parse_route import ParseRoute
        pr = ParseRoute()
        pr.parse_route(outdir,xlsx_file)
        no_overwrite = False
        if len(route_id) > 1:
            raise ValueError("Only one route can be passed to update_shapes")
        route_id = route_id[0]

        route = Route.objects.get(route_id=route_id)

        # Find Services in route
        trips = Trip.objects.filter(route=route) # Need to find trips to find related services
        if len(trips) == 0:
            self.stdout.write(self.style.ERROR('--No trips found for route %s'%route))

        # Find unique stop sequences
        unique_stop_dir_trips, unique_stop_dir_seq = self.get_unique_stop_direction_seqs(trips)
        print "============================"
        print "update_shapes: Found %d sets of trips that share stops and direction" % len(unique_stop_dir_seq)

        # For each unique stop_dir sequence, check shape status and generate a new one if needed
        shape_ids = self.get_shape_ids(trips)
        for seq_trips, seq in zip(unique_stop_dir_trips, unique_stop_dir_seq):
            print "update_shapes: Processing the set of trips that travel from stop_id %s to %s with direction %s" % \
                  (seq[0], seq[-2], seq[-1])
            # Check if all trips in seq have same shape
            seq_shape_ids = self.get_shape_ids(seq_trips) # get shape ids for seq of trips
            if len(set(seq_shape_ids)) > 1:
                raise ValueError("  Trips with same stop sequence have different values for shape_id.\n"+
                                 "  Please modify your route xlsx so that the following trips have same shape_id\n"+
                                 "  (or empty shape_id)\n"+
                                 "  Trips:"+str([t.trip_id for t in seq_trips]))
            seq_shape_id = list(set(seq_shape_ids))[0]
            force_update = False
            if seq_shape_id is None or force_update:  # If no shape exists for that stop_sequence, create it.
                seq_shape_id = self.find_unique_shape_id(route, seq, existing_names=shape_ids)
                print '  -> Creating new shape named %s' % seq_shape_id
                from shape_generator import ShapeGenerator
                sg = ShapeGenerator()
                if 'SIMPLE_SHAPE' in [t.extra_data for t in trips]:
                    shape_method = 'simple'
                elif 'WALKING_SHAPE' in [t.extra_data for t in seq_trips]:
                    shape_method = 'walking'
                else:
                    shape_method = 'driving'
                shape_points = sg.find_shape_points(seq, seq_shape_id, shape_method)
                # shape_points = sg.get_shape_points_example() # Used for testing
                outdir = os.path.join(settings.GTFS_FOLDER)
                fname = os.path.join(outdir, seq_shape_id + '.geojson')
                feed = Feed.objects.all()[0] # todo: organize feeds
                shape = sg.create_shape_object(shape_points, feed)
                for trip in seq_trips:  # Assign new shape to corresponding trips
                    trip.shape = shape
                    trip.save()
                sg.dump_shape_to_geojson(outdir, shape)
                sg.show_shape_geojsonio(fname)
                #break
            else: # Shape exists in database
                # Check if geojson file exists for the shape referred in the trip set
                shape_file = os.path.join(outdir, seq_shape_id+".geojson")
                if not os.path.isfile(shape_file):
                    raise ValueError("File %s not found in %s, but is referenced in some trips of route %s\n" %
                                     (shape_file, outdir, route_id) +
                                     "Remove references to file to create the shape again")
                else:
                     print '  -- update_shapes: shape "%s" found. No need to do anything with '% seq_shape_id +\
                           'this set of trips'

        # Dump route xlsx file with updated references to shapes
        from dump_route import DumpRoute
        dr = DumpRoute()
        dr.dump_route(route, no_overwrite)
        import sys
        sys.exit()

        print 'imported'

    def get_shape_ids(self, trips):
        ''' Return shape_ids for the trips'''
        shape_ids = []
        for trip in trips:
            if trip.shape is not None:
                shape_ids.append(trip.shape.shape_id)
            else:
                shape_ids.append(None)
        return shape_ids

    def get_unique_stop_direction_seqs(self, trips):
        ''' For each trip, get its stop + direction sequence.
        Return unique sets of sequences
        Input: list of trips
        Output:
        list of unique_stop_dir_trips (trips that have same seq as corresponding element in unique_stop_dir_seq
        list of unique_stop_dir_sequence, (given as stop ids, direction)
        '''
        stop_dir_seq = []
        for trip in trips:
            # Find stops for that trip
            st=StopTime.objects.filter(trip=trip).order_by('stop_sequence')
            s_ids = [str(e.stop.stop_id) for e in st]
            #print 'trip %s, s_ids %s' % (trip, s_ids)
            s_ids.append('d'+str(trip.direction)) # Set as txt to avoid confusion with stop_id=0
            stop_dir_seq.append((s_ids))
        unique_stop_dir_seq = [list(x) for x in set(tuple(x) for x in stop_dir_seq)]

        # For every seq in unique_stop_dir_seq, find trips
        unique_stop_dir_trips = []
        for seq in unique_stop_dir_seq:
            # Find istop_dir_seqndices of trips that belong to seq:
            indexes = [i for i,x in enumerate(stop_dir_seq) if x == seq]
            unique_stop_dir_trips.append([trips[i] for i in indexes])
        return unique_stop_dir_trips, unique_stop_dir_seq

    def find_unique_shape_id(self, route, stop_dir_seq, existing_names):
        ''' Generate a unique shape name '''
        g = itertools.count()
        while(True):
            name = 'shape_%s_%s_%s' % (str(route.short_name), str(stop_dir_seq[-1]), str(g.next()))
            if name not in existing_names:
                return name

