from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Agency, Service, Route,
                              Stop, StopTime, Trip, Frequency, Shape, ShapePoint)
from difflib import ndiff
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math

class Command(BaseCommand):

    help = 'Dump given route(s) to excel files'

    def add_arguments(self, parser):
        parser.add_argument('route_id', nargs='+', type=str)
        #parser.add_argument('exact_match', nargs='+', type=str)

    def handle(self, *args, **options):
        dr = DumpRoute()
        route_id_arg = options['route_id'][0]
        #exact_match = int(options['exact_match'][0])

        if '*' not in route_id_arg:
            routes = [Route.objects.get(route_id=route_id_arg)]
        elif route_id_arg == '*':
            routes = Route.objects.all()
        else:
            raise ValueError("dump_route argument must be either a valid route name or '*'")

        for route in routes:
            try:
                dr.dump_route(route)
            except ValueError, e:
                print "--- Something went wrong with route %s:\n   >> %s" % (route.route_id, e)
                pass

class DumpRoute():
    def dump_route(self, route, no_overwrite=False):
        # print '--dump_file: Dumping route %s'%route

        outdir = os.path.join(settings.GTFS_FOLDER)
        if no_overwrite:
            outfile = os.path.join(outdir, 'route_'+str(route.route_id)+"_updated"+".xlsx")
        else:
            outfile = os.path.join(outdir, 'route_'+str(route.route_id)+".xlsx")
        excel_writer = pd.ExcelWriter(outfile)

        # Dump route info
        info_df = pd.DataFrame(columns=['agency', 'short_name', 'long_name', 'desc', 'rtype', 'url', 'color',
                                        'text_color'])
        agency = route.agency.id if route.agency != None else ""
        info_df = info_df.append({
            'agency': agency,
            'short_name': route.short_name,
            'long_name': route.long_name,
            'desc': route.desc,
            'rtype': route.rtype,
            'url': route.url,
            'color': route.color,
            'text_color': route.text_color},
            ignore_index=True)
        info_df.to_excel(excel_writer, sheet_name='info', index=False)

        # Find Services in route
        trips = Trip.objects.filter(route=route) # Need to find trips to find related services
        services_id = set([t.service_id for t in trips]) # Find services in the trip set. Refers to pk.
        services = Service.objects.filter(id__in=services_id)

        if len(trips) == 0:
            self.stdout.write(self.style.ERROR('--No trips found for route %s'%route))

        for service in services:
            self.dump_service(route, service, excel_writer)
            #break

        # Dump shapes
        from shape_generator import ShapeGenerator
        SG = ShapeGenerator()
        for shape in Shape.objects.filter(trip__in=trips).distinct():
            SG.dump_shape_to_geojson(outdir, shape)

        if no_overwrite:
            print "--dump_file: created file %s with updated references to shapes. \n" % outfile +\
                "Copy those references to the original routes file, or just replace it with %s" % outfile
        else:
            backup = backup_file(outdir, outfile)
            if backup is None:
                print "--dump_file: created file %s" % outfile
            else:
                print "--dump_file: Updated contents of file %s \n" % outfile +\
                      "             Previous contents have been backed up as %s" % backup
        excel_writer.save()

    def dump_service(self, route, service, excel_writer):
        '''
        Find which directions are associated to the trips
        Find trips and stops related to that route-service combination
        Find common stops
        '''
        #import time
        #start_time = time.time()
        #print("--- start directions loop %s seconds ---" % (time.time() - start_time))

        all_trips = Trip.objects.filter(route=route).filter(service=service)
        # Fill-in header cells
        columns = ['direction_id', 'stop_name', 'stop_id', 'arr/dep']
        t = pd.DataFrame(columns=columns)
        t.loc['headsign', 'arr/dep'] = 'headsign (empty | headsign) -->'
        t.loc['block', 'arr/dep'] = 'block (empty | block_id) -->'
        t.loc['shapes', 'arr/dep'] = 'shape (empty | shape_id) -->'
        t.loc['wheelchair_accessible', 'arr/dep'] = 'wheelchair_accessible (empty | 1 | 2) -->'
        t.loc['bikes_allowed', 'arr/dep'] = 'bikes_allowed (empty | 1 | 2) -->'
        t.loc['extra_data', 'arr/dep'] = 'extra_data (empty|WALKING_SHAPE) -->'

        t.loc['headsign', 'stop_name'] = '_' # Required for parsed to set dtype to string
        t.loc['headsign', 'stop_id'] = '_' # Required for parsed to set dtype to string

        directions = set([trip.direction for trip in all_trips])  # Find services in the trip set
        directions = list(directions)
        directions.sort()

        for direction in directions:
            #print 'dump_route: dumping route %s, service %s, direction %s'%(
            #    route.route_id, service.service_id, direction)
            # Fill-in header rows
            ordered_trips_by_direction = list(self.get_ordered_trips(route, service, direction));
            common_stops = self.get_common_trip_stops(ordered_trips_by_direction)
            for trip in ordered_trips_by_direction:  # Needed later on to preserve column order
                columns.append(trip.trip_id)
            for trip in ordered_trips_by_direction:
                # Fill-in trip header rows
                t[trip.trip_id] = None  # Insert column
                if trip.headsign is not None:
                    t.loc['headsign', trip.trip_id] = trip.headsign
                if trip.block is not None:
                    t.loc['block', trip.trip_id] = trip.block.block_id
                if trip.shape is not None:
                    t.loc['shapes', trip.trip_id] = trip.shape.shape_id
                t.loc['wheelchair_accessible', trip.trip_id] = trip.wheelchair_accessible
                t.loc['bikes_allowed', trip.trip_id] = trip.bikes_allowed

            # For each stop, create the corresponding row(s)
            # Row height depends on how many fields are needed for that stop (only arr/dep, or maybe pickup fields)
            for stop in common_stops:  # Iterate across stop objects
                # Find maximum common number of fields
                fields = self.get_common_stoptime_fields(stop, ordered_trips_by_direction)
                for idx, field in enumerate(fields):  # Add a row for each field
                    row = pd.DataFrame(index=[t.shape[0]+1])
                    if idx == 0:
                        row['direction_id'] = direction
                        row['stop_name'] = stop.name
                        row['stop_id'] = stop.stop_id
                    row['arr/dep'] = field
                    for trip in ordered_trips_by_direction: # Fill in row for all trips
                        stoptime = StopTime.objects.filter(stop=stop, trip=trip)
                        if len(stoptime) == 0: # Case for which stoptime does not exist in trip
                            continue
                        stoptime = stoptime[0]
                        if (field == 'arr/dep' or field == 'arr') and\
                            stoptime.arrival_time is not None:
                            row[trip.trip_id] = str(stoptime.arrival_time)
                        elif field == 'dep' and\
                            stoptime.departure_time is not None:
                            row[trip.trip_id] = str(stoptime.departure_time)
                        elif field == 'stop_headsign':
                            row[trip.trip_id] = stoptime.stop_headsign
                        elif field == 'pickup_type':
                            row[trip.trip_id] = stoptime.pickup_type
                        elif field == 'drop_off_type':
                            row[trip.trip_id] = stoptime.drop_off_type
                    t = t.append(row) # Append row to the overall table

        # Create four row for each possible frequency. Index is frequency_idx
        num_frequencies = self.get_num_frequencies(all_trips)
        rows = pd.DataFrame() # assign colums
        for idx in xrange(num_frequencies):
            rows.loc[idx*4+0, 'arr/dep'] = 'frequency_start_time'
            rows.loc[idx*4+1, 'arr/dep'] = 'frequency_end_time'
            rows.loc[idx*4+2, 'arr/dep'] = 'frequency_headway_secs'
            rows.loc[idx*4+3, 'arr/dep'] = 'frequency_exact_times'
        # Dump frequencies
        if num_frequencies !=0:
            for trip in all_trips:
                for idx, frequency in enumerate(self.get_trip_frequencies(trip)):
                    rows.loc[idx*4+0, trip.trip_id] = str(frequency.start_time)
                    rows.loc[idx*4+1, trip.trip_id] = str(frequency.end_time)
                    rows.loc[idx*4+2, trip.trip_id] = str(frequency.headway_secs)
                    rows.loc[idx*4+3, trip.trip_id] = str(frequency.exact_times)
        t = t.append(rows)

        print('--dump_file: Dumped route %s, service %s, %d directions and %d trips' %
                              (route.route_id, service.service_id, len(directions), len(all_trips)))
        t = t[columns]  # Set table in the right order
        t.to_excel(excel_writer, sheet_name=service.service_id, index=False)

    def get_common_trip_stops(self, trips):
        ''' Returns a list of alls unique stops given a list of trips. The list is organized according to
        stop_sequence. If a trip has more stops than another, the extra stops are
        inserted in the right place, after the last common stop and before the first common stop
        after the detour.
        Input: list of trips
        Output: Stop sequence (given as stop ids)
        '''
        common_stops = []
        for trip in trips:
            # Find stops for that trip
            st=StopTime.objects.filter(trip=trip).order_by('stop_sequence')
            s_ids = [str(e.stop.stop_id) for e in st]
            diffs = []
            # TODO: substitute comparison by ndiff with difflib.SequenceMatcher
            for e in ndiff(common_stops, s_ids):
                if e[0] != '?':  # Remove description of intraline differences
                    diffs.append(e[2:])
            common_stops = diffs
            #print 'common_stops2', common_stops

        # for e in common_stops: print e
        #for stop_id in common_stops:
        #    print stop_id, Stop.objects.get(stop_id=stop_id)
        return [Stop.objects.get(stop_id=e) for e in common_stops]

    def get_num_frequencies(self, trips):
        ''' Returns the maximum number of trip frequencies
        '''
        max_frequencies = 0
        for trip in trips:
            f = Frequency.objects.filter(trip=trip)
            if len(f) > max_frequencies:
                max_frequencies = len(f)
        return max_frequencies

    def get_ordered_trips(self, route, service, direction=None):
        if direction is not None:
            trips = Trip.objects.filter(route=route). \
                filter(service=service). \
                filter(direction=direction)
        else:
            trips = Trip.objects.filter(route=route). \
                filter(service=service)
        # Sort trips by their earlier departure time.
        # First, create a list with dep times for each trip (to be reused later)
        trip_stop_times = []
        first_dep_time = []
        #pass trips.order_by()
        for t in trips:
            # Find stops for that trip
            trip_stop_times.append(StopTime.objects.filter(trip=t).earliest('stop_sequence'))
            first_dep_time.append(trip_stop_times[-1].departure_time)
        ordered_stop_times = self.sort_lists(first_dep_time, trip_stop_times)
        ordered_trips = [st.trip for st in ordered_stop_times]
        # for t in trips:
        #     # Find stops for that trip
        #     trip_stop_times.append(
        #         StopTime.objects.filter(trip=t).order_by('departure_time'))
        #     first_dep_time.append(self.get_trip_stop_times(t)[0].departure_time)
        # ordered_trips = self.sort_lists(first_dep_time, trips)
        return ordered_trips

    def get_trip_stop_times(self, trip):
        return StopTime.objects.filter(trip=trip).order_by('stop_sequence')

    def get_trip_frequencies(self, trip):
        return Frequency.objects.filter(trip=trip).order_by('start_time')

    def get_common_stoptime_fields(self, stop, trips):
        # For each stop, determine the common set of arr/dep, arr, dep, pickup fields for all trips,
        fields = []
        for trip in trips:
            stoptime = StopTime.objects.filter(stop=stop, trip=trip)
            if len(stoptime) == 0:  # Case where that stop does not exist for this trip
                continue
            elif len(stoptime) > 1:
                raise ValueError("Trips that stop twice in same stop in the same direction are not supported.\n"+\
                                 "      See stoptimes in trip %s at stops %s"%(
                                 trip, ", ".join([s.stop.stop_id for s in stoptime])))
            else:
                stoptime = stoptime[0]
            if stoptime.arrival_time == stoptime.departure_time:
                fields.append('arr/dep')
            else:
                if stoptime.arrival_time != "":
                    fields.append('arr')
                if stoptime.departure_time != "":
                    fields.append('dep')
            if stoptime.stop_headsign != "":
                fields.append('stop_headsign')
            if stoptime.pickup_type != "":
                fields.append('pickup_type')
            if stoptime.drop_off_type != "":
                fields.append('drop_off_type')
        fields = set(fields)  # Find unique values
        if ('arr' in fields or 'dep' in fields) and 'arr/dep' in fields:
            # In this case, the arr/dep simplification cannot be done
            fields.add('arr')
            fields.add('dep')
            fields.remove('arr/dep')

        # Ensure fields are correctly ordered
        desired_order = ['arr/dep', 'arr', 'dep', 'stop_headsign', 'pickup_type', 'drop_off_type']
        fields = sorted(fields, key=lambda x: desired_order.index(x))  # returns list
        return fields

    def sort_lists(self, a, b):
        ''' Sort a list 'b' according to 'a' '''
        if len(b) == 0:
            return b
        # print len(a), len(b)
        z = sorted(zip(a, b), key=lambda x: x[0])
        sb = zip(*z)[1]
        return sb

def backup_file(outdir, outfile):
    """ Creates backup folder if needed, then saves backup of outfile"""
    import os
    if not os.path.isfile(outfile): # return if backup is not needed
        return None
    else:
        outdir_backup = os.path.join(settings.GTFS_FOLDER, 'backup')
        if not os.path.isdir(outdir_backup):
            os.makedirs(outdir_backup)
        outfile_base= os.path.splitext(os.path.split(outfile)[1])[0] # extract route name
        outfile_backup = os.path.join(outdir_backup, outfile_base+"_backup"+".xlsx")
        from shutil import copyfile
        copyfile(outfile, outfile_backup)
        return outfile_backup
