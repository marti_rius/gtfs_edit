from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import Route, Trip, Service, StopTime, Feed, Agency, Stop,  Block, Shape, Frequency, ShapePoint
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math
from utils import check_field

class Command(BaseCommand):

    help = 'Parses route xlsx file and dumps content to database'

    def add_arguments(self, parser):
        parser.add_argument('route_id', nargs='+', type=str)

    def handle(self, *args, **options):
        '''
        '''
        outdir = os.path.join(settings.GTFS_FOLDER)
        pr = ParseRoute()
        import glob
        glob_pattern = os.path.join(settings.GTFS_FOLDER, 'route_' + options['route_id'][0] + '.xlsx')
        xlsx_files = glob.glob(glob_pattern)

        for xlsx_file in xlsx_files:
            pr.parse_route(outdir, xlsx_file)

class ParseRoute():
    def parse_route(self, outdir, xlsx_file):
        self.xlsx_file = xlsx_file
        print "Parsing route file %s" % xlsx_file
        basename = os.path.splitext(os.path.split(xlsx_file)[-1])[0] # get base name for file (route_xxx)
        route_id = basename.split('route_')[1]

        sheets = pd.read_excel(xlsx_file, sheet_name = None)
        info = sheets['info']

        try:
            route = Route.objects.get(route_id=route_id)  # Check if already in database
            route.delete()
        except Route.MultipleObjectsReturned:
            self.stdout.write(self.style.SUCCESS('--ERROR: More than one Route with route_id=%s in database. ') %
                              route_id)
            raise ValueError, ""
        except Route.DoesNotExist:
            route = Route()

        route.feed = Feed.objects.all()[0] # There must be only one feed
        if check_field(info['agency'][0]):
            try:
                route.agency = Agency.objects.get(name=info['agency'][0])
            except Agency.DoesNotExist:
                route.agency = ""
        route.route_id = route_id
        route.short_name = info['short_name'][0] # Need to select even if there is only one row
        route.long_name = info['long_name'][0]
        if check_field(info['desc'][0]):
            route.desc = info['desc'][0]
        route.rtype = info['rtype'][0]
        if check_field(info['url'][0]):
            route.url = info['url'][0]
        if check_field(info['color'][0]):
            route.color = info['color'][0]
        if check_field(info['text_color'][0]):
            route.text_color = info['text_color'][0]
        route.save()
        del sheets['info']

        self.first_row = 5  # Row where first stop_name appears. Note that table has headers and starts at 0
        self.first_col = 4  # Col where the first trip appears
        # Iterate along service tabs
        for service_id in sheets.keys():
            service = Service.objects.get(service_id=service_id)
            self.table = sheets[service_id]
            self.table=self.table.fillna("")
            self.fill_stop_ids()
            self.parse_trips(route, service)
        self.parse_shapes(route, outdir)

    def parse_shapes(self, route, outdir):
        ''' Parse shape objects referenced in route, service trips '''
        trips = Trip.objects.filter(route=route)
        shapes = Shape.objects.filter(trip__in=trips).distinct()
        for shape in shapes:
            fname = os.path.join(outdir, shape.shape_id+'.geojson')
            if not os.path.isfile(fname):
                raise ValueError("File %s not found in %s, but is referenced in some trips of route %s\n" %
                                 (shape_file, outdir, route_id) +
                                 "Remove references to file and run update_shapes to create the shape")
            from shape_generator import ShapeGenerator
            SG = ShapeGenerator()
            SG.parse_shape_from_geojson(outdir, shape.shape_id)
        print '--: Parsed %d shapes for route %s' % (len(shapes), route)

    def parse_trips(self, route, service):
        ''' Parse each trip in the table '''
        trip_num = 0
        freq_trips_num = 0 # Trips that use frequency fields
        for col in self.table.columns:
            if col in ['direction_id', 'stop_name', 'stop_id', 'arr/dep']: # Skip non-trip columns
                continue
            trip = Trip()
            trip.trip_id = col
            trip.route = route
            trip.service = service
            if check_field(self.table[col].iloc[0]):
                trip.headsign = self.table[col].iloc[0]
            block_id = self.table[col].iloc[1]
            if check_field(block_id):  # Valid block specified
                block = list(Block.objects.filter(block_id=block_id))
                if len(block) == 0:
                    block = Block()
                    block.feed = Feed.objects.all()[0]
                    block.block_id = block_id
                    block.save()
                elif len(block) == 1:
                    block = block[0]
                elif len(block) > 1:
                    self.stdout.write(self.style.ERROR('ERROR: multiple blocks named %s' % block_id))
                trip.block = block

            shape_id = self.table[col].iloc[2]
            if check_field(shape_id):
                shape = Shape.objects.filter(shape_id=shape_id)
                if len(shape) == 0:
                    shape = Shape()
                    shape.feed = Feed.objects.all()[0]
                    shape.shape_id = shape_id
                    shape.save()
                elif len(shape) == 1:
                    shape = shape[0]
                elif len(shape) > 1:
                    self.stdout.write(self.style.ERROR('ERROR: multipe shapes named %s' % shape_id))
                trip.shape = shape

            if check_field(self.table[col].iloc[3]):
                trip.wheelchair_accessible = self.table[col].iloc[3]
            if check_field(self.table[col].iloc[4]):
                trip.bikes_allowed = self.table[col].iloc[4]
            if check_field(self.table[col].iloc[5]):
                trip.extra_data = self.table[col].iloc[5]
            # find first cell with a valid stop_time, this will set the direction
            get_direction = True
            for idx, group in enumerate(self.group_trip_data(col)): # Iterate over stop_times and frequencies
                #print '----idx',route, service, col, st_idx, self.table['stop_id'].iloc[st_idx]
                if group.iloc[0]['type'] == 'stop_time':
                    if get_direction: # Assign trip direction based on first non-empty stoptime
                        trip.direction = int(group.iloc[0]['direction_id'])
                        trip.save()
                        get_direction = False
                    stoptime = StopTime()
                    stoptime.trip = trip
                    stoptime.stop = Stop.objects.get(stop_id=group.iloc[0]['stop_id'])
                    if 'arr/dep' in group['arr/dep'].values:
                        stoptime.arrival_time = group.loc[group['arr/dep'] == 'arr/dep', col].iloc[0]
                        stoptime.departure_time = group.loc[group['arr/dep'] == 'arr/dep', col].iloc[0]
                    if 'arr' in group['arr/dep'].values:
                        stoptime.arrival_time = group.loc[group['arr/dep'] == 'arr', col].iloc[0]
                    if 'dep' in group['arr/dep'].values:
                        stoptime.departure_time = group.loc[group['arr/dep'] == 'arr', col].iloc[0]
                    if 'stop_headsign' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'stop_headsign', col].iloc[0]):
                        stoptime.stop_headsign = group.loc[group['arr/dep'] == 'stop_headsign', col].iloc[0]
                    if 'pickup_type' in group['arr/dep'].values and\
                        check_field(group.loc[group['arr/dep'] == 'pickup_type', col].iloc[0]):
                        stoptime.pickup_type = group.loc[group['arr/dep'] == 'pickup_type', col].iloc[0]
                    if 'drop_off_type' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'drop_off_type', col].iloc[0]):
                        stoptime.drop_off_type = group.loc[group['arr/dep'] == 'drop_off_type', col].iloc[0]
                    stoptime.stop_sequence = idx  # Just needs to be an increasing integer
                    stoptime.save()

                # Process frequency
                elif group.iloc[0]['type'] == 'frequency':
                    frequency = Frequency()
                    frequency.trip = trip
                    if 'frequency_start_time' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'frequency_start_time', col].iloc[0]):
                        frequency.start_time = group.loc[group['arr/dep'] ==
                                                                   'frequency_start_time', col].iloc[0]
                    if 'frequency_end_time' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'frequency_end_time', col].iloc[0]):
                        frequency.end_time = group.loc[group['arr/dep'] ==
                                                                   'frequency_end_time', col].iloc[0]
                    if 'frequency_headway_secs' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'frequency_headway_secs', col].iloc[0]):
                        frequency.headway_secs = group.loc[group['arr/dep'] ==
                                                                   'frequency_headway_secs', col].iloc[0]
                    if 'frequency_exact_times' in group['arr/dep'].values and\
                            check_field(group.loc[group['arr/dep'] == 'frequency_exact_times', col].iloc[0]):
                        frequency.exact_times = group.loc[group['arr/dep'] ==
                                                                   'frequency_exact_times', col].iloc[0]
                    frequency.save()
                    freq_trips_num += 1
            trip.save()
            trip_num += 1
        print '--: Parsed %d trips for route %s in file %s, service %s, with a total of %d frequency definitions' %\
                                            (trip_num, route.short_name, self.xlsx_file, service.service_id,
                                             freq_trips_num)

    def group_trip_data(self, trip_col):
        """ Groups data for a given trip. Each group can be:
        -stop_times
        -frequency
        stop_times can be single a single arr/dep time or separate arr and dep times
        frequency can have optional exact_times field or not
        """
        tmp = self.table[['direction_id', 'stop_id', 'arr/dep', trip_col]].iloc[self.first_row:] # tmp table
        groups = []
        processing_stop_time = False
        processing_frequency = False
        for idx, row in tmp.iterrows():
            # stop_times are grouped by a) stop_id/dir change, b) last row, c) frequency field found
            if processing_stop_time:
                if (row['stop_id'] != "" and row['stop_id'] != stop_id) or \
                   (row['direction_id'] != "" and row['direction_id'] != direction_id) or \
                   (row['arr/dep'] == "frequency_start_time"): # end of stop time
                    processing_stop_time = False
                    if any(group[trip_col]): # check that at least one field is not empty
                        group['type'] = 'stop_time'
                        groups.append(group)
                else:
                    group = group.append(row)
            elif processing_frequency:
                if row['arr/dep'] == "frequency_start_time": # end of stop time
                    processing_frequency = False
                    if any(group[trip_col]): # check that at least one field is not empty
                        groups.append(group)
                else:
                    group['type'] = 'frequency'
                    group = group.append(row)

            if row['stop_id'] != "" and not processing_stop_time: # stop_id found, then it is a stop_time group
                processing_stop_time = True
                group = pd.DataFrame()
                stop_id = row['stop_id'] # preserve to detect changes in future rows
                direction_id = row['direction_id'] # preserve to detect changes in future rows
                group = group.append(row)
        # frequency groups end at end of table or when another frequency_start_time is found
            elif row['arr/dep'] == 'frequency_start_time' and not processing_frequency: # frequency group
                processing_frequency = True
                group = pd.DataFrame()
                group = group.append(row)
        if len(group) != 0 and any(group[trip_col]):  # check that at least one field is not empty
            groups.append(group)
        return groups

    def fill_stop_ids(self):
        ''' Fill stop_id column in a table from stop_names
        stop_id must be specified. If stop_name is specified as well, then they are checked for match'''
        for idx, row in self.table.iterrows():
            if idx < self.first_row:
                continue
            if row.stop_id == "": # Empty in case of 'frequency' fields
                continue
            row.stop_id = str(row.stop_id)
            #print "fill_stop_id, stop_id", row.stop_id
            row.stop_id = row.stop_id.replace(" ", "") # Eliminates spaces, difficult to see in Excel
            # Deal with possible mismatches between stop_name and stop_ids
            stop_name_by_id = Stop.objects.get(stop_id=row.stop_id).name
            if row.stop_name != "" and row.stop_name != stop_name_by_id:
                raise ValueError(self.style.ERROR('WARNING: stop_id %s should have name %s but has %s'%
                                 (row.stop_id, stop_name_by_id, row.stop_name)))