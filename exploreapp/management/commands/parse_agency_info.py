from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Agency, Feed, Service)
import pandas as pd
import os.path
from django.conf import settings
from utils import check_field

class Command(BaseCommand):

    help = 'Parses agency_info.xlsx file and dumps info to database'

    def handle(self, *args, **options):
        infile = os.path.join(settings.GTFS_FOLDER,'agency_info.xlsx')
        s = pd.read_excel(infile, sheet_name = None)

        # Set Agency
        agency_id = s['agency'].agency_id[0]
        try:
            agency=Agency.objects.get(agency_id=agency_id) # Check if agency_id already in database
            self.stdout.write(self.style.SUCCESS('--Agency with agency_id=%s already exists in database. Updating')%
                              agency_id)
        except Agency.MultipleObjectsReturned:
            self.stdout.write(self.style.ERROR('--ERROR: More than one Agency with agency_id=%d in database. ') %
                          agency_id)
            raise ValueError, ""
        except Agency.DoesNotExist:
            agency = Agency() # New agency
        if check_field(s['agency'].agency_id[0]):
            agency.agency_id = s['agency'].agency_id[0] # Need to select even if there is only one row
        agency.feed = Feed.objects.all()[0] # There must be only one feed
        agency.name = s['agency'].agency_name[0]
        agency.url = s['agency'].agency_url[0]
        agency.timezone = s['agency'].agency_timezone[0]
        if check_field(s['agency'].agency_lang[0]):
            agency.lang = s['agency'].agency_lang[0]
        if check_field(s['agency'].agency_phone[0]):
            agency.phone = s['agency'].agency_phone[0]
        agency.save()
        self.stdout.write(self.style.SUCCESS('--Successfully parsed Agency info'))

        # Set Service
        for _, row in s['calendar'].iterrows():
            try:
                service = Service.objects.get(service_id=row.service_id)  # Check if agency_id already in database
                self.stdout.write(
                    self.style.SUCCESS('--Service with service_id=%s already exists in database. Updating') %
                    row.service_id)
            except Service.MultipleObjectsReturned:
                self.stdout.write(self.style.ERROR('--ERROR: More than one Service with service_id=%s in database. ') %
                                  row.service_id)
                raise ValueError, ""
            except Service.DoesNotExist:
                service = Service()  # New service

            service.feed = Feed.objects.all()[0]
            service.service_id = row.service_id
            service.monday = row.monday
            service.tuesday = row.tuesday
            service.wednesday = row.wednesday
            service.thursday = row.thursday
            service.friday = row.friday
            service.saturday = row.saturday
            service.sunday = row.sunday
            service.start_date = row.start_date
            service.end_date = row.end_date
            service.save()
        self.stdout.write(self.style.SUCCESS('--Successfully parsed Service info for %d services')%
            len(s['calendar']))


