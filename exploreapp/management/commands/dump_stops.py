from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Agency, Service, Route,
                              Stop, StopTime, Trip)
from difflib import ndiff
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math

class Command(BaseCommand):

    help = 'Dump stops info to excel file'

    def handle(self, *args, **options):
        outdir = os.path.join(settings.GTFS_FOLDER)

        stops = list(Stop.objects.all())
        outfile = os.path.join(outdir, 'stops.xlsx')

        # Dump each stop in one row. This makes multiple agencies more readable
        stops_df=pd.DataFrame(columns=['id', 'feed', 'stop_id', 'stop_code', 'stop_name', 'stop_desc', 'stop_lat',\
                                       'stop_lon', 'zone_id', 'stop_url', 'location_type', 'parent_station',\
                                       'stop_timezone', 'wheelchair_boarding', 'extra_data'])
        for stop in stops:
            extra_data = stop.extra_data if stop.extra_data != {} else ""
            stops_df = stops_df.append({
                'id': stop.id,
                'feed': stop.feed.id,
                'stop_id': stop.stop_id,
                'stop_code': stop.code,
                'stop_name': stop.name,
                'stop_desc': stop.desc,
                'stop_lat': stop.point.y,  # Inverse order in multigtfs
                'stop_lon': stop.point.x,
                'zone_id': stop.zone,
                'stop_url': stop.url,
                'location_type': stop.parent_station,
                'stop_timezone': stop.timezone,
                'wheelchair_boarding': stop.wheelchair_boarding,
                'extra_data': extra_data
                }, ignore_index=True)

        stops_df.to_excel(outfile, sheet_name='stops', index=False)
        self.stdout.write(self.style.SUCCESS('--Dumped stops info for %s stops') % len(stops))
