
import math
import numpy as np

def check_field(field):
    """Checks correctness of a field.
    If it is float, it should not be Nan
    If it is unicode, it should not be empty
    Returns True if field is not empty"""
    if type(field) in [float, np.float64]:
        return not math.isnan(field)
    elif type(field) in [unicode, str]:
        return field != ""
    elif type(field) in [int, np.int64]:
        return field
    else:
        raise ValueError('Wrong type for field')

