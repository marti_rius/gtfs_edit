from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Stop)
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math
from utils import check_field

class Command(BaseCommand):

    help = 'Parses stops.xlsx file and dumps content to database'

    def handle(self, *args, **options):
        #try:
        xlsx_file = os.path.join(settings.GTFS_FOLDER,'stops.xlsx')
        stops_df = pd.read_excel(xlsx_file)
        new_stops = 0
        for idx, row in stops_df.iterrows():
            # Set Stop
            try:
                stop=Stop.objects.get(stop_id=row.stop_id)  # Check if already in database
                self.stdout.write(
                    self.style.SUCCESS('--Stop with stop_id=%d already exists in database. Updating') %
                    row.stop_id)
            except Stop.MultipleObjectsReturned:
                self.stdout.write(self.style.ERROR('--ERROR: More than one Stop with agency_id=%d in database. ') %
                                  row.stop_id)
                raise ValueError, ""
            except Stop.DoesNotExist:
                stop = Stop()
            stop.stop_id = row.stop_id
            stop.feed = Feed.objects.all()[0] # There must be only one feed
            stop.name = row.stop_name
            stop.point = Point(float(row.stop_lon),float(row.stop_lat))  # Inverse order in multigtfs
            if check_field(row.stop_code):
                stop.code = row.stop_code
            if check_field(row.stop_desc):
                stop.desc = row.stop_desc
            if check_field(row.zone_id):
                stop.zone_id = row.zone_id
            if check_field(row.stop_url):
                stop.url = row.stop_url
            if check_field(row.location_type):
                stop.location_type = row.location_type
            if check_field(row.parent_station):
                stop.parent_station = row.parent_station
            if check_field(row.stop_timezone):
                stop.stop_timezone = row.stop_timezone
            if check_field(row.wheelchair_boarding):
                stop.wheelchair_boarding = row.wheelchair_boarding
            stop.save()
            new_stops += 1
        self.stdout.write(self.style.SUCCESS('--Successfully parsed stops info for %d stops') %
                          (new_stops))
