from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Agency, Service, Route,
                              Stop, StopTime, Trip)
import pandas as pd
import os.path
from django.conf import settings

class Command(BaseCommand):

    help = 'Dump agency info to excel file'

    def handle(self, *args, **options):
        outdir = os.path.join(settings.GTFS_FOLDER)

        agencies = list(Agency.objects.all())
        print agencies
        outfile = os.path.join(outdir, 'agency_info.xlsx')
        excel_writer = pd.ExcelWriter(outfile)

        # Dump each agency in one row. This makes multiple agencies more readable
        # Column list needed to preserve order
        columns = ['agency_id', 'agency_name', 'agency_url', 'agency_timezone', \
                   'agency_lang', 'agency_phone', 'agency_fare_url', 'agency_extra_data']
        info_df = pd.DataFrame()
        for agency in agencies:
            extra_data = agency.extra_data if agency.extra_data != {} else ""
            info_df = info_df.append({
                #'agency_feed': agency.feed.id,
                'agency_id': agency.agency_id,
                'agency_name': agency.name,
                'agency_url': agency.url,
                'agency_timezone': agency.timezone,
                'agency_lang': agency.lang,
                'agency_phone': agency.phone,
                'agency_fare_url': agency.fare_url,
                'agency_extra_data': extra_data
                }, ignore_index=True)

        info_df = info_df[columns]
        info_df.to_excel(excel_writer, sheet_name='agency', index=False)

        # Dump calendar info
        # Dump each service in one row. This makes multiple services more readable
        calendar_df=pd.DataFrame(columns=['service_id', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', \
                                          'saturday', 'sunday', 'start_date', 'end_date'])
        for service in Service.objects.all():
            calendar_df = calendar_df.append({
                'service_id': service.service_id,
                'monday': service.monday,
                'tuesday': service.tuesday,
                'wednesday': service.wednesday,
                'thursday': service.thursday,
                'friday': service.friday,
                'saturday': service.saturday,
                'sunday': service.sunday,
                'start_date': service.start_date,
                'end_date': service.end_date
                }, ignore_index=True)

        calendar_df.to_excel(excel_writer, sheet_name='calendar', index=False)
        excel_writer.save()

        self.stdout.write(self.style.SUCCESS('--Dumped agency info for %s agencies') % len(agencies))
