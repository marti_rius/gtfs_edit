from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Stop, Route, Agency)
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math

class Command(BaseCommand):

    help = 'Parses route_*.xlsx files in edit_folders_in and dumps content to database'

    def add_arguments(self, parser):
        parser.add_argument('feed_id', nargs='+', type=str)

    def handle(self, *args, **options):
        '''
        '''
        feed_id=options['feed_id'][0]
        #import pdb; pdb.set_trace()
        if feed_id == '*':
            to_delete=Feed.objects.all()
        else:
            to_delete=Feed.objects.filter(id = int(feed_id))
        if len(to_delete) != 0:
            to_delete.delete()

        # Create a new, single feed
        feed = Feed()
        feed.name = 'Feed1'
        feed.id = 1
        feed.save()

        self.stdout.write(self.style.SUCCESS('--Successfully deleted %d feeds') % len(to_delete))
