from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Agency, Service, Route,
                              Stop, StopTime, Trip, Shape, ShapePoint)
import itertools
from difflib import ndiff
import pandas as pd
import os.path
from django.conf import settings
from django.contrib.gis.geos import Point
import math

class ShapeGenerator():

    help = 'Create shapes for given route(s). Uses Google Directions API to suggest a shape from the stops'

    # Notes on Points:
    # In GTFS the storage is lat, lon
    # In Google maps API is lat, lon
    # However, in multi_gtfs and in geojson the order is lon, lat.

    def find_shape_points(self, stop_dir_seq_arg, shape_id, shape_method):
        """
        Divide stop_dir_seq into chucks so that the stop sequence can be passed to google directions API
        to find the connecting route.
        Returns shape as a list of tuples containing two coordinates and eventually the travelled_distance
        """
        # TODO: HANDLE SHAPE METHOD SIMPLE
        google_api_key = settings.GOOGLE_API_KEY

        if shape_method not in ['driving', 'walking']:
            raise ValueError('Shape method %s not supported' % shape_method)
        simpl_method = 'normal' if shape_method == 'driving' else 'coarse'
        # Build shapes
        import polyline, googlemaps
        import more_itertools
        max_waypoints = 12  # It is 10 + start & endpoints
        from copy import copy
        stop_dir_seq = copy(stop_dir_seq_arg) # Prevent modification of input argument
        _ = stop_dir_seq.pop() # Remove direction
        stops = [Stop.objects.get(stop_id=stop_id) for stop_id in stop_dir_seq]
        # Group stops in sets of max_waypoints and call api to get shape
        full_shape = []  # Container for the full shape
        # Get portions of trip, of which the last point of one portion is the first point
        # of the next one
        for portion in more_itertools.windowed(stops, max_waypoints, None, max_waypoints-1):
            portion = list(portion)
            try:
                portion=portion[0:portion.index(None)] # Remove None padding for last element(s)
            except ValueError:
                pass
            # Prepare data for submittal to the API
            origin_id, destination_id = portion[0], portion[-1]
            waypoints_id = portion[1:-1]
            #print "origin %s, waypoints %s, dest %s"%(origin_id, waypoints_id, destination_id)
            origin = (origin_id.point.y, origin_id.point.x)  # Inversed in multigtfs
            destination = (destination_id.point.y, destination_id.point.x)
            waypoints = [(stop.point.y, stop.point.x) for stop in waypoints_id]
            polyline.encode(waypoints)
            gmaps = googlemaps.Client(key=google_api_key)
            directions_result = gmaps.directions(origin=origin,
                                         destination=destination,
                                         waypoints=waypoints,
                                         mode=shape_method)
            # TODO: check error code
            # Iterate over legs, then over steps, to be able to extract distances info from the later
            route = directions_result[0]
            dist_traveled = 0  # Distance traveled
            for ileg, leg in enumerate(route['legs']):
                for istep, step in enumerate(leg['steps']):
                    #print "leg %s, step %s"%(ileg, istep)
                    polyline_points=polyline.decode(step['polyline']['points'])
                    #print 'polyline_points',polyline_points
                    if len(polyline_points) >1:
                        polyline_points=polyline_points[:-1]
                    # Reduce number of points in polyline
                    simpl_polyline_points = self.simplify_coords(polyline_points, simpl_method)  
                    simpl_polyline_points[0]=simpl_polyline_points[0]+[dist_traveled,] # Add distance to first point
                    full_shape.extend(simpl_polyline_points)
                    dist_traveled += step['distance']['value'] # Distance travelled of first point of next step
                    #print 'appended_polyline:', polyline_points
                    #print 'dist_traveled:',dist_traveled

            # todo: simplify coords to be compatible with travelled distance

            full_shape2 = [] # Adding shape_id and shape_pt_sequence
            for pt_seq, elem in enumerate(full_shape):
                shape_point = (shape_id, elem[0], elem[1], pt_seq)
                if len(elem)==3: # travelled distance available
                     shape_point += (elem[2],)
                full_shape2.append(shape_point)
        # todo: correctly accumulate and set units of travelled_distance
        #print self.show_shape(stops, full_shape2)
        return full_shape2


    def show_shape_geojsonio(self, fname):
        from geojsonio import display
        print '  OPENING SHAPE VIEW IN WEB BROWSER - This may take a few seconds\n' + \
              '  > If you are happy with the shape, just close browser window.                           <\n' + \
              '  > If you want to modify it, edit shape and save to edit folder with Save->GeoJSON       <\n' + \
              '  > Then rename it to %s.                                      <' % fname

        with open(fname) as f:
            contents = f.read()
            display(contents)
        raw_input("  --- Press Enter to continue... ---")

    def create_shape_object(self, shape_points, feed):
        shape_id = shape_points[0][0]
        shape = Shape.objects.filter(shape_id=shape_id)
        if len(shape) > 0:  # Remove preexisting shape
            Shape.objects.filter(shape_id=shape_id).delete()
        shape = Shape()
        shape.feed = feed
        shape.shape_id = shape_id
        shape.save()
        l = []
        for point in shape_points:
            if len(point) == 4:
                l.append(ShapePoint(shape=shape,
                                point=Point(float(point[2]), float(point[1])),
                                sequence=point[3]))
            else:
                l.append(ShapePoint(shape=shape,
                                    point=Point(float(point[2]), float(point[1])),
                                    sequence=point[3],
                                    traveled=point[4]))
        ShapePoint.objects.bulk_create(l)
        return shape

    def simplify_coords(self, coords, method, epsilon=0.000000001):
        if method == 'coarse':
            epsilon = 1
        from simplification.cutil import simplify_coords, simplify_coords_vw
        #scoords = simplify_coords(zzcoords, 0.00001)
        zcoords=zip(*coords) # Remove eventual 3rd element of coord tuples that contains position
        zzcoords=zip(zcoords[0],zcoords[1])
        scoords = simplify_coords_vw(zzcoords, epsilon)
        #print "simplify_coords: lens before %d, after %d ,epsilon %d"%(len(coords), len(scoords), epsilon)
        return scoords

    def parse_shape_from_geojson(self, outdir, shape_name):
        ''' Parses shape in geojson format'''

        import geojson
        geojson_file = os.path.join(outdir, shape_name+'.geojson')
        with open(geojson_file) as f:
            shape_data = geojson.loads(f.read())

        shape = Shape.objects.get(shape_id=shape_name)
        ShapePoint.objects.filter(shape=shape).delete()

        # Identify feature whose geometry is 'LineString'
        linestring = None
        for feature in shape_data['features']:
            if feature['geometry']['type'] == 'LineString':
                linestring = feature
                break
        if feature is None:
            raise ValueError('Shape %s does not contain any feature whose geometry->type is LineString')

        z=zip(feature['geometry']['coordinates'],
              feature['properties']['sequence'],
              feature['properties']['traveled'])
        l = []
        for point, sequence, traveled in z:
            l.append(ShapePoint(shape=shape,
                                point=Point(point[0], point[1]),
                                sequence=sequence,
                                traveled=traveled))
        ShapePoint.objects.bulk_create(l)

    def dump_shape_to_geojson(self, outdir, shape):
        ''' Dumps shape and stop points to geojson format, so that it can be edited in QGIS
        Input: outdir, shape object, optionally: stop locations to ease shape edition
        Output: geojson file where:
               Stops is a FeatureCollection of Features containing a Point, and whose properties has a 'name' with
               the name of the stop.
               Shapes is a Feature with a linestring with the shape points
        '''
        import geojson
        feat_list = []
        # Dump stops
        # Find stops sequence that corresponds to the shape
        stops = \
            Stop.objects.filter(stoptime__trip__shape=shape).order_by('stoptime__stop_sequence').distinct()
        for stop in stops[:]:  # Dump stops
            point = geojson.Point((stop.point.x, stop.point.y))
            feat_properties = {"name": '%s-%s' % (stop.id, stop.name)}
            feat = geojson.Feature(geometry=point,
                                   properties=feat_properties)
            feat_list.append(feat)
        # Dump points
        coord_list, sequence, traveled = [], [], []
        for point in ShapePoint.objects.filter(shape=shape).order_by('sequence'):
            coord_list.append((point.point.x, point.point.y))
            sequence.append(point.sequence)
            traveled.append(point.traveled)
        feat_properties = {"name": '%s' % shape.shape_id,
                           "sequence" : sequence,
                           "traveled" : traveled
                           }
        feat = geojson.Feature(geometry=geojson.LineString(coord_list),
                               properties=feat_properties)
        feat_list.append(feat)

        feat_col = geojson.FeatureCollection(feat_list)
        fname = os.path.join(outdir, shape.shape_id+'.geojson')
        with open(fname, 'w') as f:
            geojson.dump((feat_col), f)
        print 'dump_shape_to_geojson: dumped file %s' % fname
        return fname

    def show_shape_googlemaps(self, stops, full_shape):
        ''' Show the shape for visual verification
        Input: stops: stop list, full_shape: list of tuples with shape points
        Output: google static maps URL
        '''
        # create url for viewing map
        import string
        from motionless import DecoratedMap, LatLonMarker
        if len(stops) != (len(set(stops))):
            print "WARNING: some stops are repeated. This may create shape visualization problems.\n",\
                  "         If this route returns to the departing point, consider splitting it\n",\
                  "         into two directions"
        origin = stops[0]
        destination = stops[-1]
        interm_stops = stops[1:-2]

        road_styles = [{
            'feature': 'road.highway',
            'element': 'geomoetry',
            'rules': {
                'visibility': 'simplified',
                'color': '#c280e9'
            }
        }, {
            'feature': 'transit.line',
            'rules': {
                'visibility': 'simplified',
                'color': '#bababa'
            }
        }]
        dmap = DecoratedMap(style=road_styles, size_x=640, size_y=640, scale=4)

        dmap.add_marker(LatLonMarker(origin.point.y, origin.point.x, label='1', color='green'))
        dmap.add_marker(LatLonMarker(destination.point.y, destination.point.x, label='2', color='red'))
        # TODO: think how all that is called from the rest of code
        g = itertools.cycle(itertools.product(['blue', 'yellow', 'violet', 'orange'], string.ascii_uppercase))
        #print "Label stop_id stop_name"
        for i, stop in enumerate(interm_stops): # stops
            color_letter=g.next()
            dmap.add_marker(LatLonMarker(stop.point.y,stop.point.x, label=color_letter[1],
                                         color=color_letter[0], size='mid'))
            print color_letter[1],color_letter[0],' - ', stop.id, ' ', stop.name
        for point in full_shape: # shape
            dmap.add_path_latlon(point[1], point[2])
        return (dmap.generate_url())

    def get_shape_points_example(self):
        return [('shape_L3_d1_1', 41.46994, 2.09063, 0, 0), ('shape_L3_d1_1', 41.47005, 2.09063, 1),
                ('shape_L3_d1_1', 41.47013, 2.09058, 2), ('shape_L3_d1_1', 41.47019, 2.09046, 3),
                ('shape_L3_d1_1', 41.4702, 2.09035, 4), ('shape_L3_d1_1', 41.47018, 2.09026, 5),
                ('shape_L3_d1_1', 41.4701, 2.09014, 6), ('shape_L3_d1_1', 41.47003, 2.09011, 7),
                ('shape_L3_d1_1', 41.47001, 2.09011, 8, 74), ('shape_L3_d1_1', 41.46998, 2.0894, 9),
                ('shape_L3_d1_1', 41.46996, 2.0891, 10), ('shape_L3_d1_1', 41.46993, 2.08875, 11),
                ('shape_L3_d1_1', 41.46991, 2.0884, 12), ('shape_L3_d1_1', 41.46986, 2.08793, 13),
                ('shape_L3_d1_1', 41.46982, 2.08763, 14), ('shape_L3_d1_1', 41.46975, 2.08735, 15),
                ('shape_L3_d1_1', 41.46972, 2.08727, 16), ('shape_L3_d1_1', 41.46969, 2.08718, 17, 248),
                ('shape_L3_d1_1', 41.47062, 2.08638, 18), ('shape_L3_d1_1', 41.47108, 2.08603, 19),
                ('shape_L3_d1_1', 41.47138, 2.08577, 20, 222), ('shape_L3_d1_1', 41.47143, 2.08584, 21),
                ('shape_L3_d1_1', 41.47149, 2.08587, 22), ('shape_L3_d1_1', 41.47156, 2.08587, 23),
                ('shape_L3_d1_1', 41.47165, 2.0858, 24), ('shape_L3_d1_1', 41.47169, 2.08569, 25),
                ('shape_L3_d1_1', 41.47168, 2.08559, 26), ('shape_L3_d1_1', 41.47168, 2.08557, 27, 55),
                ('shape_L3_d1_1', 41.47166, 2.08551, 28), ('shape_L3_d1_1', 41.47159, 2.08544, 29),
                ('shape_L3_d1_1', 41.47151, 2.08542, 30), ('shape_L3_d1_1', 41.47145, 2.08544, 31),
                ('shape_L3_d1_1', 41.4714, 2.08549, 32), ('shape_L3_d1_1', 41.47135, 2.08563, 33),
                ('shape_L3_d1_1', 41.47136, 2.08573, 34), ('shape_L3_d1_1', 41.4714, 2.08581, 35),
                ('shape_L3_d1_1', 41.47146, 2.08585, 36), ('shape_L3_d1_1', 41.47147, 2.08586, 37, 75),
                ('shape_L3_d1_1', 41.47148, 2.08592, 38), ('shape_L3_d1_1', 41.47169, 2.08639, 39),
                ('shape_L3_d1_1', 41.47176, 2.08666, 40), ('shape_L3_d1_1', 41.47182, 2.08691, 41, 96),
                ('shape_L3_d1_1', 41.47211, 2.08687, 42, 33), ('shape_L3_d1_1', 41.47209, 2.08678, 43),
                ('shape_L3_d1_1', 41.47306, 2.08665, 44), ('shape_L3_d1_1', 41.47363, 2.08657, 45),
                ('shape_L3_d1_1', 41.47402, 2.08651, 46), ('shape_L3_d1_1', 41.47461, 2.08638, 47),
                ('shape_L3_d1_1', 41.47495, 2.08629, 48), ('shape_L3_d1_1', 41.4757, 2.08603, 49),
                ('shape_L3_d1_1', 41.47588, 2.08594, 50), ('shape_L3_d1_1', 41.47639, 2.0857, 51),
                ('shape_L3_d1_1', 41.47668, 2.08551, 52), ('shape_L3_d1_1', 41.4768, 2.08542, 53),
                ('shape_L3_d1_1', 41.477, 2.08524, 54), ('shape_L3_d1_1', 41.47718, 2.08505, 55),
                ('shape_L3_d1_1', 41.47722, 2.08502, 56), ('shape_L3_d1_1', 41.47737, 2.08484, 57),
                ('shape_L3_d1_1', 41.47746, 2.08472, 58, 641), ('shape_L3_d1_1', 41.47751, 2.08473, 59),
                ('shape_L3_d1_1', 41.47756, 2.08465, 60), ('shape_L3_d1_1', 41.47755, 2.08459, 61),
                ('shape_L3_d1_1', 41.47788, 2.08411, 62)]
