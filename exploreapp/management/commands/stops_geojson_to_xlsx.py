from django.core.management.base import BaseCommand, CommandError

from multigtfs.models import (Feed, Stop)
import pandas as pd
import os.path
from django.conf import settings

from django.contrib.gis.db import models
from django.contrib.gis.geos import Point

class Command(BaseCommand):

    help = 'Parses stops.kml file in edit_folders and dumps content to stops.xlsx'

    def handle(self, *args, **options):
        #try:
        geojson_file = os.path.join(settings.GTFS_FOLDER, 'stops.geojson')

        import geojson
        with open(geojson_file) as f:
            stops = geojson.loads(f.read())

        stops_df = pd.DataFrame(columns=[
                            'stop_id',
                            'stop_code',
                            'stop_name',
                            'stop_desc',
                            'stop_lat',
                            'stop_lon',
                            'zone_id',
                            'stop_url',
                            'location_type',
                            'parent_station',
                            'stop_timezone',
                            'wheelchair_boarding',
                            ])

        i=0
        for stop in stops['features']:
            name = stop['properties']['name']
            x, y = stop['geometry']['coordinates']
            id = stop['properties']['id']
            print name, x, y, id

            my_stop = {
                'stop_id': id,
                'stop_name': name,
                'stop_lat': y,
                'stop_lon': x
                }
            stops_df = stops_df.append(my_stop, ignore_index=True)
            i +=1
        xlsx_file = os.path.join(settings.GTFS_FOLDER, 'stops_from_geojson.xlsx')
        stops_df.to_excel(xlsx_file, sheet_name='Stops')
        self.stdout.write(self.style.SUCCESS('--Successfully created file %s for %d stops') %
                          (xlsx_file, i))

        
        

        


