# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.contrib.gis.db import models

class Agency(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey('Feed')
    agency_id = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=200)
    timezone = models.CharField(max_length=255)
    lang = models.CharField(max_length=2)
    phone = models.CharField(max_length=255)
    fare_url = models.CharField(max_length=200)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'agency'

class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=80)
    class Meta:
        managed = False
        db_table = 'auth_group'

class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')
    class Meta:
        managed = False
        db_table = 'auth_group_permissions'

class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'auth_permission'

class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)
    class Meta:
        managed = False
        db_table = 'auth_user_groups'

class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)
    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'

class Block(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey('Feed')
    block_id = models.CharField(max_length=63)
    class Meta:
        managed = False
        db_table = 'block'

class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)
    action_time = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    class Meta:
        managed = False
        db_table = 'django_admin_log'

class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'django_content_type'

class DjangoSession(models.Model):
    session_key = models.CharField(max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'django_session'

class Fare(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey('Feed')
    fare_id = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=17, decimal_places=4)
    currency_type = models.CharField(max_length=3)
    payment_method = models.IntegerField()
    transfers = models.IntegerField(blank=True, null=True)
    transfer_duration = models.IntegerField(blank=True, null=True)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'fare'

class FareRules(models.Model):
    id = models.IntegerField(primary_key=True)
    fare = models.ForeignKey(Fare)
    route = models.ForeignKey('Route', blank=True, null=True)
    origin = models.ForeignKey('Zone', blank=True, null=True)
    destination = models.ForeignKey('Zone', blank=True, null=True)
    contains = models.ForeignKey('Zone', blank=True, null=True)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'fare_rules'

class Feed(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    created = models.DateTimeField()
    meta = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'feed'

class FeedInfo(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    publisher_name = models.CharField(max_length=255)
    publisher_url = models.CharField(max_length=200)
    lang = models.CharField(max_length=20)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    version = models.CharField(max_length=255)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'feed_info'

class Frequency(models.Model):
    id = models.IntegerField(primary_key=True)
    trip = models.ForeignKey('Trip')
    start_time = models.IntegerField()
    end_time = models.IntegerField()
    headway_secs = models.IntegerField()
    exact_times = models.CharField(max_length=1)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'frequency'

class Route(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    route_id = models.CharField(max_length=255)
    agency = models.ForeignKey(Agency, blank=True, null=True)
    short_name = models.CharField(max_length=63)
    long_name = models.CharField(max_length=255)
    desc = models.TextField()
    rtype = models.IntegerField()
    url = models.CharField(max_length=200)
    color = models.CharField(max_length=6)
    text_color = models.CharField(max_length=6)
    geometry = models.MultiLineStringField(blank=True, null=True)
    extra_data = models.TextField(blank=True)
    objects = models.GeoManager()
    class Meta:
        managed = False
        db_table = 'route'

class Service(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    service_id = models.CharField(max_length=255)
    monday = models.BooleanField()
    tuesday = models.BooleanField()
    wednesday = models.BooleanField()
    thursday = models.BooleanField()
    friday = models.BooleanField()
    saturday = models.BooleanField()
    sunday = models.BooleanField()
    start_date = models.DateField()
    end_date = models.DateField()
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'service'

class ServiceDate(models.Model):
    id = models.IntegerField(primary_key=True)
    service = models.ForeignKey(Service)
    date = models.DateField()
    exception_type = models.IntegerField()
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'service_date'

class Shape(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    shape_id = models.CharField(max_length=255)
    geometry = models.LineStringField(blank=True, null=True)
    objects = models.GeoManager()
    class Meta:
        managed = False
        db_table = 'shape'

class ShapePoint(models.Model):
    id = models.IntegerField(primary_key=True)
    shape = models.ForeignKey(Shape)
    point = models.PointField()
    sequence = models.IntegerField()
    traveled = models.FloatField(blank=True, null=True)
    extra_data = models.TextField(blank=True)
    objects = models.GeoManager()
    class Meta:
        managed = False
        db_table = 'shape_point'

class Stop(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    stop_id = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    point = models.PointField()
    zone = models.ForeignKey('Zone', blank=True, null=True)
    url = models.CharField(max_length=200)
    location_type = models.CharField(max_length=1)
    parent_station = models.ForeignKey('self', blank=True, null=True)
    timezone = models.CharField(max_length=255)
    wheelchair_boarding = models.CharField(max_length=1)
    extra_data = models.TextField(blank=True)
    objects = models.GeoManager()
    class Meta:
        managed = False
        db_table = 'stop'

class StopTime(models.Model):
    id = models.IntegerField(primary_key=True)
    trip = models.ForeignKey('Trip')
    stop = models.ForeignKey(Stop)
    arrival_time = models.IntegerField(blank=True, null=True)
    departure_time = models.IntegerField(blank=True, null=True)
    stop_sequence = models.IntegerField()
    stop_headsign = models.CharField(max_length=255)
    pickup_type = models.CharField(max_length=1)
    drop_off_type = models.CharField(max_length=1)
    shape_dist_traveled = models.FloatField(blank=True, null=True)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'stop_time'

class Transfer(models.Model):
    id = models.IntegerField(primary_key=True)
    from_stop = models.ForeignKey(Stop)
    to_stop = models.ForeignKey(Stop)
    transfer_type = models.IntegerField()
    min_transfer_time = models.IntegerField(blank=True, null=True)
    extra_data = models.TextField(blank=True)
    class Meta:
        managed = False
        db_table = 'transfer'

class Trip(models.Model):
    id = models.IntegerField(primary_key=True)
    route = models.ForeignKey(Route)
    service = models.ForeignKey(Service, blank=True, null=True)
    trip_id = models.CharField(max_length=255)
    headsign = models.CharField(max_length=255)
    short_name = models.CharField(max_length=63)
    direction = models.CharField(max_length=1)
    block = models.ForeignKey(Block, blank=True, null=True)
    shape = models.ForeignKey(Shape, blank=True, null=True)
    geometry = models.LineStringField(blank=True, null=True)
    wheelchair_accessible = models.CharField(max_length=1)
    bikes_allowed = models.CharField(max_length=1)
    extra_data = models.TextField(blank=True)
    objects = models.GeoManager()
    class Meta:
        managed = False
        db_table = 'trip'

class Zone(models.Model):
    id = models.IntegerField(primary_key=True)
    feed = models.ForeignKey(Feed)
    zone_id = models.CharField(max_length=63)
    class Meta:
        managed = False
        db_table = 'zone'

